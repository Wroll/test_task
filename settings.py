"""
This Modules handles env variables
"""

import os
from typing import Optional

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

USERNAME_1: Optional[str] = os.environ.get('USERNAME_1')
PASSWORD_1: Optional[str] = os.environ.get('PASSWORD_1')
BASE_URL: Optional[str] = os.environ.get('BASE_URL')
