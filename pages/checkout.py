"""
Module to holding functionalities for Checkout pages
"""
from pages.base import Base, Locator


class CheckoutYourInformation(Base):
    """
    Class to holding functionalities for 'Checkout: Your Information' page
    """

    @property
    def first_name_input(self) -> Locator:
        """
        First name locator
        :return: locator
        """
        return self.page.locator("css=input#first-name")

    @property
    def last_name_input(self) -> Locator:
        """
        Last name locator
        :return: locator
        """
        return self.page.locator("css=input#last-name")

    @property
    def postal_code_input(self) -> Locator:
        """
        Postal code locator
        :return: locator
        """
        return self.page.locator("css=input#postal-code")

    @property
    def continue_button(self) -> Locator:
        """
        Continue button locator
        :return: locator
        """
        return self.page.locator("css=input#continue")

    def enter_your_information(self, first_name: str, last_name: str,
                               postal_code: str) -> None:
        """
        Method to fill a form 'your information'

        :param first_name: first name
        :param last_name: last name
        :param postal_code: postal code

        :return: None
        """
        self.first_name_input.fill(first_name)
        self.last_name_input.fill(last_name)
        self.postal_code_input.fill(postal_code)

    def click_continue(self) -> 'CheckoutOverview':
        """
        Click Continue button

        :return: CheckoutOverview
        """
        self.continue_button.click()
        return CheckoutOverview(self.page)


class CheckoutOverview(Base):
    """
    Class to holding functionalities for 'Checkout: Overview' page
    """

    def find_product_by_product_title(self, product_title: str) -> Locator:
        """
        Method to find product on Checkout Overview page using product title

        :param product_title: product title

        :return: locator
        """
        return self.page.locator(
            f"xpath=//div[contains(text(), '{product_title}')]")

    @property
    def total(self) -> Locator:
        """
        Total price locator

        :return: locator
        """
        return self.page.locator("xpath=//div[@class='summary_info_label summary_total_label']")

    @property
    def finish(self) -> Locator:
        """
        Finish button locator

        :return: locator
        """
        return self.page.locator("css=button#finish")

    def get_total_price(self) -> float:
        """
        Get total price

        :return: total price
        """
        text = self.total.inner_text()
        price = text.split()[1][
                1:]  # parsed price without text and dollar sign
        return float(price)

    def click_finish(self) -> 'CheckoutComplete':
        """
        Method to click on button Finish
        :return:
        """
        self.finish.click()
        return CheckoutComplete(self.page)


class CheckoutComplete(Base):
    """
    Class to holding functionalities for 'Checkout: Complete' page
    """

    @property
    def complete_header(self) -> Locator:
        """
        Successful order message locator

        :return: locator
        """
        return self.page.locator("css=h2")
