"""
Module to holding functionalities for Products page
"""
from pages.base import Base, Locator
from utils import create_value_id_attr


class Products(Base):
    """
    Class to holding functionalities for Products page
    """

    @property
    def products_title(self) -> Locator:
        """
        Products title locator

        :return: locator
        """
        return self.page.locator("xpath=//span[@class='title']")

    def add_to_cart_product_by_title(self, product_title) -> None:
        """
        Method to add product to the cart

        :param product_title: product title

        :return: None
        """
        id_value = create_value_id_attr(product_title, 'add')
        self.page.locator(f"xpath=//button[@id="f"'{id_value}']").click()
