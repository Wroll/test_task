"""
Module to holding functionalities for Your Cart page
"""
from pages.base import Base, Page, Locator
from pages.checkout import CheckoutYourInformation
from utils import create_value_id_attr


class YourCart(Base):
    """
    Class to holding functionalities for Your Cart  page
    """
    url: str

    def __init__(self, page: Page):
        super().__init__(page)
        self.url = 'cart'

    @property
    def product_item_in_cart(self) -> Locator:
        """
        Locator for product item in cart

        :return: locator
        """
        return self.page.locator("xpath=//div[@class='cart_item_label']")

    @property
    def checkout_button(self) -> Locator:
        """
        Locator for checkout button

        :return: locator
        """
        return self.page.locator("xpath=//button[@id='checkout']")

    def get_product_title_locator_by_product_title(self, product_title: str) \
            -> Locator:
        """
        Method to get product title locator by title
        :param product_title: product title

        :return: locator
        """
        return self.page.locator(f"xpath=//div[contains(text(), "
                                 f"'{product_title}')]")

    def remove_product_by_product_title(self, product_title: str) -> None:
        """
        Remove product from cart by product name

        :param product_title: product title

        :return: None
        """
        id_value = create_value_id_attr(product_title, 'remove')
        self.page.locator(f"xpath=//button[@id='{id_value}']").click()

    def click_checkout(self) -> CheckoutYourInformation:
        """
        Click checkout button

        :return: CheckoutYourInformation object
        """
        self.checkout_button.click()
        return CheckoutYourInformation(self.page)
