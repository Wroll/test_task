"""
Module to holding functionalities for Login page
"""
from pages.base import Base, Page, Locator


class Login(Base):
    """
    Class to holding functionalities for Login page
    """
    password: str
    username: str

    def __init__(self, page: Page, username: str, password: str):
        super().__init__(page)
        self.username = username
        self.password = password

    @property
    def password_field(self) -> Locator:
        """
        Password field locator

        :return: locator
        """
        return self.page.get_by_placeholder("Password")

    @property
    def username_field(self) -> Locator:
        """
        Username field locator

        :return: locator
        """
        return self.page.get_by_placeholder("Username")

    @property
    def login_button(self) -> Locator:
        """
        Login button locator

        :return: locator
        """
        return self.page.locator("xpath=//input[@id='login-button']")

    @property
    def error_msg(self) -> Locator:
        """
        Error message locator

        :return: locator
        """
        return self.page.locator("xpath=//h3")

    def login_to_market(self) -> None:
        """
        Method to log in to market

        :return: None
        """
        self.username_field.fill(self.username)
        self.password_field.fill(self.password)
        self.login_button.click()

    def navigate(self) -> 'Login':
        """
        Navigate to the Login page

        :return: Login object
        """
        self.base_url: str
        self.page.goto(self.base_url)
        return self
