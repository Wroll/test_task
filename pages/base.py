"""
Module that represents base UI functionality
"""
from typing import Optional

from playwright.sync_api import Page
from playwright.sync_api._generated import Locator
from settings import BASE_URL


class Base:
    """
    Class that represents base UI methods
    """
    base_url: Optional[str]
    page: Page

    def __init__(self, page: Page):
        self.page = page
        self.base_url = BASE_URL

    def navigate_to(self, partial_url: str) -> None:
        """
        Navigate to page using partial url.

        :param partial_url:
        :return: None
        """
        self.page.goto(f"{self.base_url}/{partial_url}.html")

    @property
    def title(self) -> Locator:
        """
        Locator for title

        :return: locator
        """
        return self.page.locator("css=span.title")
