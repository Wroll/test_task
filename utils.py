"""
This module holding functionalities for general usage
"""

import logging


def create_value_id_attr(product_title: str, action: str) -> str:
    """
    Method to prepare product locator using product title depends on action.
    If action is 'add' method preparing
    string value for id button 'ADD TO CART'/ Otherwise the method preparing
    string value for id button 'REMOVE'

    :param product_title: product title
    :param action: 'add' for 'ADD TO CART', 'remove' for 'REMOVE'
    :return: sting value for id attr

    :Example:

    The following example will show how method can be used

            .. code-block:: python
                    # Note: We're trying to find webelement using xpath
                    # //button[@id='add-to-cart-sauce-labs-backpack']
                    # we're creating xpath based on product title 'Sauce
                    Labs Backpack'
                    locator = self.create_value_id_attr('Sauce Labs Backpack'
                    , 'add') # returns
                    # 'add-to-cart-sauce-labs-backpack'
                    self.page.locator(f"xpath=//button[@id=
                    '{self.create_value_id_attr(product_title, 'add')}']").
                    click()
    """
    string = "-".join(product_title.lower().split())
    actions = ("add", "remove")
    if action not in actions:
        logging.error("Action is incorrect. Check method description")
    if action == 'add':
        return "add-to-cart-" + string
    return "remove-" + string
