"""
Fixtures for general usage. TBD
"""

import pytest
from settings import USERNAME_1, PASSWORD_1


@pytest.fixture
def credentials():
    """
    Fixture that returns credential from .env

    Returns dict with username and password
    -------

    """
    yield {'username': USERNAME_1, 'password': PASSWORD_1}
