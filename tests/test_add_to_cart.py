"""
This module holding 'test_add_to_cart' test to cover 'adding product item
to cart' test flow
"""

import pytest
from playwright.sync_api import Page, expect

from pages.login import Login
from pages.products import Products
from pages.your_cart import YourCart
from constants import ProductsData


@pytest.fixture
def setup_teardown(page: Page, credentials: dict):
    """
    Setup teardown fixture

    :param page: Page
    :param credentials: credential to login
    :return: data required for test
    """
    # browser = playwright.chromium.launch(headless=False)
    # page = browser.new_page()
    login_page = Login(page, **credentials)
    login_page.navigate()
    login_page.login_to_market()

    yield page


def test_add_to_cart(setup_teardown):
    """
    Test to cover 'add to cart' test flow

    :param setup_teardown: setup teardown fixture
    """
    page = setup_teardown

    products = Products(page)
    products.add_to_cart_product_by_title(ProductsData.product_title)

    your_cart = YourCart(page)
    your_cart.navigate_to(your_cart.url)

    # Checking for product name on the 'Your Cart' page
    locator = your_cart.get_product_title_locator_by_product_title(
        ProductsData.product_title)
    expect(locator).to_have_text(ProductsData.product_title)
