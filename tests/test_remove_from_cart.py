"""
This module holding 'test_remove_from_cart' test to cover 'remove product
from cart' test flow
"""

import pytest
from playwright.sync_api import Page, expect

from pages.login import Login
from pages.products import Products
from pages.your_cart import YourCart
from constants import ProductsData


@pytest.fixture
def setup_teardown(page: Page, credentials: dict):
    """
    Setup and teardown fixture
    """
    login_page = Login(page, **credentials).navigate()
    login_page.login_to_market()

    yield page


def test_remove_from_cart(setup_teardown):
    """
    Test to cover 'remove product from cart' test flow
    """
    page = setup_teardown

    # Add to the cart
    Products(page).add_to_cart_product_by_title(ProductsData.product_title)

    your_cart = YourCart(page)
    your_cart.navigate_to(your_cart.url)
    # Remove from the cart
    your_cart.remove_product_by_product_title(ProductsData.product_title)

    # Checking if product not presented in the cart
    product_locator = your_cart. \
        get_product_title_locator_by_product_title(ProductsData.product_title)
    expect(product_locator).not_to_have_text([ProductsData.product_title])
