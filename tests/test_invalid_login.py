"""
This module holding 'test_unsuccessful_login' test to cover 'invalid login'
test flow
"""

from playwright.sync_api import Page, expect

from pages.login import Login
from constants import LoginData


def test_unsuccessful_login(page: Page):
    """
    Test to cover 'invalid login' test flow
    """
    login_page = Login(page, **LoginData.INVALID_CREDENTIALS)
    login_page.navigate()
    login_page.login_to_market()

    # Checking error message when log in with invalid credentials
    expect(login_page.error_msg).to_have_text(LoginData.LOGIN_ERROR_MESSAGE)
