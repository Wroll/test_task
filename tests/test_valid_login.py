"""
This module holding 'test_successful_login' test to cover 'valid login to
the market' test flow
"""

from playwright.sync_api import Page, expect
# from playwright.sync_api import Playwright

from pages.login import Login
from pages.products import Products


def test_successful_login(page: Page, credentials: dict):
    """
    Test to cover 'valid login to the market' test flow
    """
    # browser = playwright.chromium.launch(headless=False, slow_mo=500)
    # page = browser.new_page()
    login_page = Login(page, **credentials)
    login_page.navigate()
    login_page.login_to_market()

    products = Products(page)
    expect(products.products_title).to_have_text("Products")
