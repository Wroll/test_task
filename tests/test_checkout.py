"""
This module holding 'test_checkout' test to cover 'checkout' test flow
"""
# pylint: disable=redefined-outer-name

from typing import Dict
import pytest
from playwright.sync_api import Page, expect
# from playwright.sync_api import Playwright


from pages.login import Login
from pages.products import Products
from pages.your_cart import YourCart
from constants import CheckoutData


@pytest.fixture
# def setup_teardown(playwright: Playwright, credentials: dict):
def setup_teardown(page: Page, credentials: dict):
    """
    Setup teardown fixture

    :param page: Page
    :param credentials: credential to login
    :return: data required for test
    """

    # browser = playwright.chromium.launch(headless=False)
    # page = browser.new_page()
    login_page = Login(page, **credentials)
    login_page.navigate()
    login_page.login_to_market()

    checkout_titles: Dict[str, str] = {
        'your_information': "Checkout: Your Information",
        'overview': "Checkout: Overview",
        "complete": "Checkout: Complete!"}
    product_title: str = "Sauce Labs Backpack"
    complete_header: str = "Thank you for your order!"

    yield page, product_title, checkout_titles, complete_header


def test_checkout(setup_teardown):
    """`
    Test to cover 'add to cart' test flow

    :param setup_teardown: setup teardown fixture
    """
    page, product_title, checkout_titles, complete_header = setup_teardown

    products = Products(page)
    products.add_to_cart_product_by_title(product_title)

    your_cart = YourCart(page)
    # Navigate to 'Your Cart' page
    your_cart.navigate_to(your_cart.url)
    # Moving to 'Checkout Your Information' page after clicking checkout button
    checkout_your_information = your_cart.click_checkout()

    # Check new title to make sure that we are in different page
    expect(checkout_your_information.title).to_have_text(
        checkout_titles['your_information'])

    checkout_your_information.enter_your_information(
        **CheckoutData.your_information)
    checkout_overview = checkout_your_information.click_continue()

    # Check new title to make sure that we are in different page
    expect(checkout_overview.title).to_have_text(checkout_titles['overview'])

    # Minimal validation on final checkout page. Checking product name
    # in the order and total price
    product_locator = checkout_overview.find_product_by_product_title(
        product_title)
    expect(product_locator).to_have_text(product_title)
    assert checkout_overview.get_total_price() != 0

    checkout_complete = checkout_overview.click_finish()

    # Checking message about successful checkout. Also checking title
    # to make sure that we moved to different page
    expect(checkout_complete.title).to_have_text(checkout_titles['complete'])
    expect(checkout_complete.complete_header).to_have_text(complete_header)
