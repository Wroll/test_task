"""
This module storing all static data for tests
"""
# pylint: disable=E1136

from typing import Any, Dict
from faker import Faker

fake = Faker()


class LoginData:
    """
    This class storing data for Login related tests
    """
    INVALID_CREDENTIALS: Dict[str, Any] = {'username': fake.user_name(),
                                           'password': fake.password()}
    LOGIN_ERROR_MESSAGE: str = 'Epic sadface: Username and password do not' \
                               ' match any user in this service'


class CheckoutData:
    """
    This class storing data for Checkout related tests
    """
    your_information: Dict[str, Any] = {'first_name': fake.first_name(),
                                        'last_name': fake.last_name(),
                                        'postal_code': fake.postalcode()}


class ProductsData:
    """
    This class storing data for Products related tests
    """
    product_title: str = 'Sauce Labs Backpack'
