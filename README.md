This project was created in order to resolve test task

Installation:

1. Clone the repository:
    git clone https://gitlab.com/Wroll/test_task.git

2. Create virtual env:
    https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/

3. Install dependencies:
    pip install -r requirements.txt
    
4. Install drivers for browsers:
    playwright install

4. Run the tests using command: pytest
